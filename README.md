# Repositorio del curso de Razonamiento Automatizado #

Aquí estarán disponibles los códigos vistos en clase.

### Académicos ###

* Profesora: M. en C.C. Pilar Selene Linares Arévalo
* Correo electrónico: <selene_linares@ciencias.unam.mx>
* Ayudante: L. en C.C. Fernando Abigail Galicia Mendoza
* Correo electrónico: <fernandogamen@ciencias.unam.mx>