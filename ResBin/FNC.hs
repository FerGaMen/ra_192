-- Razonamiento Automatizado
-- Ciclo: 2019-2
-- Profesora: M. en C.C. Pilar Selene Linares Arevalo
-- Ayudante: L. en C.C. Fernando Abigail Galicia Mendoza

module FNC where

-- | Form. Tipo de datos que representa una formula de la logica proposicional.
data Form = Top | Bot | V Int | Neg Form | Conj Form Form | Disy Form Form | Impl Form Form | DC Form Form deriving(Show,Eq)

-- | eIDC. Funcion que dada una formula φ devuelve una formula ψ equivalente libre de implicaciones y doble condicionales.
eIDC :: Form -> Form
eIDC φ = case φ of
  Top -> Top
  Bot -> Bot
  V x -> V x
  Neg φ1 -> Neg (eIDC φ1)
  Conj φ1 φ2 -> Conj (eIDC φ1) (eIDC φ2)
  Disy φ1 φ2 -> Disy (eIDC φ1) (eIDC φ2)
  Impl φ1 φ2 -> Disy (Neg (eIDC φ1)) (eIDC φ2)
  DC φ1 φ2 -> Conj (eIDC (Impl φ1 φ2)) (eIDC (Impl φ2 φ1))

-- | fnn'. Funcion que dada una formula φ (libre de implicaciones y doble condicionales) devuelve su Forma Normal Negativa (FNN) equivalente.
fnn' :: Form -> Form
fnn' φ = case φ of
  Top -> Top
  Bot -> Bot
  V x -> V x
  Neg φ1 -> case φ1 of
    Top -> Bot
    Bot -> Top
    V x -> Neg (V x)
    Neg φ11 -> fnn φ11
    Conj φ11 φ12 -> Disy (fnn' (Neg φ11)) (fnn' (Neg φ12))
    Disy φ11 φ12 -> Conj (fnn' (Neg φ11)) (fnn' (Neg φ12))
  Conj φ1 φ2 -> Conj (fnn' φ1) (fnn' φ2)
  Disy φ1 φ2 -> Disy (fnn' φ1) (fnn' φ2)

-- | fnn. Funcion que dada una formula φ devuelve su FNN equivalente.
fnn :: Form -> Form
fnn = fnn'.eIDC

-- | fnc'. Funcion que dada una formula φ en FNN devuelve su Forma Normal Conjuntiva (FNC) equivalente.
fnc' :: Form -> Form
fnc' φ = case φ of
  Top -> Top
  Bot -> Bot
  V x -> V x
  Neg (V x) -> Neg (V x)
  Conj φ1 φ2 -> Conj (fnc' φ1) (fnc' φ2)
  Disy φ1 φ2 -> distr (fnc' φ1) (fnc' φ2) where
    distr (Conj φ11 φ12) φ2 = Conj (distr φ11 φ2) (distr φ12 φ2)
    distr φ1 (Conj φ21 φ22) = Conj (distr φ1 φ21) (distr φ1 φ22)
    distr φ1 φ2 = Disy φ1 φ2
    
-- | fnc. Funcion que dada una formula φ devuelve su FNC equivalente.
fnc :: Form -> Form
fnc = fnc'.fnn
